#Ubuntu 20.04 is too new for cuda-10.2 (needs gcc<=9)
FROM ubuntu:18.04
MAINTAINER Maarten van Gompel <proycon@anaproy.nl>
LABEL description="Oersetter"
ENV TZ=Europe/Amsterdam
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update
RUN apt-get -y install git sudo ucto gcc g++ autoconf-archive make libtool build-essential libboost-all-dev libprotobuf10 protobuf-compiler libprotobuf-dev openssl libssl-dev libgoogle-perftools-dev gawk sed grep gnupg2 curl ca-certificates doxygen catdoc pandoc perl python3 python3-pip cargo rustc

#Install cmake from source because ubuntu package is too old
RUN cd /usr/src && git clone https://github.com/Kitware/CMake/ && cd CMake && git checkout v3.19.2 && ./bootstrap && make && sudo make install

#NVIDIA CUDA
ENV LIBRARY_PATH=/usr/local/cuda/lib64/stubs
RUN mkdir -p /run/systemd && echo 'docker' > /run/systemd/container
RUN curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - &&     echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list &&     echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list
ENV CUDA_VERSION=10.2.89
ENV CUDA_PKG_VERSION=10-2=10.2.89-1
RUN apt-get -y update && apt-get install -y --no-install-recommends cuda-cudart-$CUDA_PKG_VERSION cuda-compat-10-2 && ln -s cuda-10.2 /usr/local/cuda
RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf
ENV PATH=/usr/local/nvidia/bin:/usr/local/cuda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV LD_LIBRARY_PATH=/usr/local/nvidia/lib:/usr/local/nvidia/lib64:/usr/local/cuda/lib64
ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV NVIDIA_REQUIRE_CUDA=cuda>=10.2 brand=tesla,driver>=396,driver<397 brand=tesla,driver>=410,driver<411 brand=tesla,driver>=418,driver<419 brand=tesla,driver>=440,driver<441
ENV NCCL_VERSION=2.7.8
RUN apt-get -y update
RUN apt-get -y install --no-install-recommends cuda-libraries-$CUDA_PKG_VERSION cuda-npp-$CUDA_PKG_VERSION cuda-nvcc-$CUDA_PKG_VERSION cuda-nvtx-$CUDA_PKG_VERSION libcublas10=10.2.2.89-1 libnccl2=$NCCL_VERSION-1+cuda10.2 cuda-compiler-$CUDA_PKG_VERSION
RUN apt-get install -y --no-install-recommends     cuda-nvml-dev-$CUDA_PKG_VERSION     cuda-command-line-tools-$CUDA_PKG_VERSION     cuda-nvprof-$CUDA_PKG_VERSION     cuda-npp-dev-$CUDA_PKG_VERSION     cuda-libraries-dev-$CUDA_PKG_VERSION     cuda-minimal-build-$CUDA_PKG_VERSION     libcublas-dev=10.2.2.89-1     libnccl-dev=2.7.8-1+cuda10.2   cuda-cudart-dev-$CUDA_PKG_VERSION   cuda-curand-dev-$CUDA_PKG_VERSION cuda-cusparse-dev-$CUDA_PKG_VERSION cuda-cusolver-dev-$CUDA_PKG_VERSION
RUN apt-mark hold libnccl2 libnccl-dev
ENV CUDNN_VERSION=8.0.5.39
RUN apt-get install -y --no-install-recommends     libcudnn8=$CUDNN_VERSION-1+cuda10.2     libcudnn8-dev=$CUDNN_VERSION-1+cuda10.2     && apt-mark hold libcudnn8

RUN mkdir -p /usr/src/

#hunalign
RUN cd /usr/src && git clone https://github.com/danielvarga/hunalign.git
RUN cd /usr/src/hunalign/src/hunalign && make && cp hunalign /usr/bin

#Marian-NMT
RUN cd /usr/src && git clone https://github.com/marian-nmt/marian.git
RUN cd /usr/src/marian && git checkout 1.10.0 && mkdir -p build && cd build && cmake .. -DUSE_SENTENCEPIECE=on -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda && make -j 4
RUN cp /usr/src/marian/build/marian* /usr/local/bin/
RUN cp /usr/src/marian/build/spm_* /usr/local/bin/

#Moses SMT (we don't need the decoder technically but we do need some of its scripts)
RUN cd /usr/src && git clone https://github.com/moses-smt/mosesdecoder

#Subword Neural Machine Translation (preprocessing to segment text into subword units)
RUN sudo pip3 install subword-nmt

#Evaluation scripts
RUN sudo pip3 install sacrebleu

#Install ssam
RUN cargo install ssam && cp -f /root/.cargo/bin/ssam /usr/local/bin/ssam

#fastalign
RUN cd /usr/src && git clone https://github.com/clab/fast_align.git
RUN cd /usr/src/fast_align && mkdir build && cd build &&  cmake .. && make && cp fast_align /usr/bin/ && cp atools /usr/bin/

#Set locales
RUN apt-get -y install language-pack-en language-pack-nl
ENV LC_ALL=en_US.UTF-8

#Output defaults to profile template in case other users are generated
RUN echo "export PATH=/usr/local/nvidia/bin:/usr/local/cuda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" > /etc/skel/.profile
RUN echo "export LD_LIBRARY_PATH=/usr/local/nvidia/lib:/usr/local/nvidia/lib64:/usr/local/cuda/lib64" >> /etc/skel/.profile
RUN echo "export LANG=en_US.UTF-8" >> /etc/skel/.profile
RUN echo "export LC_ALL=en_US.UTF-8" >> /etc/skel/.profile
RUN echo "export ENV NVIDIA_VISIBLE_DEVICES=all" >> /etc/skel/.profile
RUN echo "export NVIDIA_DRIVER_CAPABILITIES=compute,utility" >> /etc/skel/.profile
RUN echo "source ~/.bashrc" >> /etc/skel/.profile

CMD /bin/bash -l
