# Oersetter 2 Pipeline

This repository contains two training pipelines for Frisian<->Dutch Machine Translation:

1. NMT (Neural Machine Translation), powered by Marian (requires a GPU)
2. SMT (Phrase-based Statistical Machine Translation), powered by Moses

The models are not included in the repository but need to be downloaded separately by invoking the following script:

``./download-models.sh``

This must be done before continuing with any of the below instructions.

For each a docker container can be made:

1. ``docker build .``
2. ``docker build -f smt.Dockerfile``

The following scripts provide an entry point on certain known hosts:

1. ``run-interactive-nmt.sh``
2. ``run-interactive-smt.sh``

In the respective containers, you can run the training pipeline as follows:

1. ``scripts/train-nmt.sh -s nl -t fy``
2. ``scripts/train-smt.sh -s nl -t fy``

For the reverse translation direction, simply adapt the parameters.
