# Experiments

## Baseline old oersetter


* **BLEU nl-fy:** 69.0
* **BLEU fy-nl:** 59.5

**NOTE**: There is a possibly large bias because part of what is now test material has been
used as training material in the old PB-SMT system! (the train/test distribution is not the same accross the systems).
So these figures should be taken lightly.

Sets (md5):

```
e02a0bc78419e23919cd8db75af66bf1  fy.test.txt
75702fc8479291f7f53f862c885558fb  nl.test.txt
```

## New SMT Baseline 3

Unlike the above, this new baseline is a fair comparison and also includes new training data collected until May 2021
(without filtering). This is comparable with experiment 3:

* **BLEU nl-fy: 49.0**

## Experiment 1

Simple NMT transformer run, no backwards model

* **BLEU nl->fy:** 52.3
* **BLEU fy->nl:** 55.0


Sets (md5):

```
9326a232d1933ab812831b63ea0ae62e  fy.dev.txt
e02a0bc78419e23919cd8db75af66bf1  fy.test.txt
5dd0c5d8f191dd0dc2c50485098ed550  fy.train.txt
1e6e51ec3f257db519d0f162f3ca61c9  nl.dev.txt
75702fc8479291f7f53f862c885558fb  nl.test.txt
12dae29faa42f293430bcc8fdefb986f  nl.train.txt
```

## Experiment 2

NMT run with backwards model enriching training input:

* **BLEU nl-fy:** 27.9  (something clearly went wrong here)

Sets (md5) WITHOUT synthetic enrichment:

```
9326a232d1933ab812831b63ea0ae62e  fy.dev.txt
e02a0bc78419e23919cd8db75af66bf1  fy.test.txt
5dd0c5d8f191dd0dc2c50485098ed550  fy.train.txt
1e6e51ec3f257db519d0f162f3ca61c9  nl.dev.txt
75702fc8479291f7f53f862c885558fb  nl.test.txt
12dae29faa42f293430bcc8fdefb986f  nl.train.txt
```

Number of lines (sentences) with and without enrichment from monolingual source:

```
$ wc -l *bpe
      500 fy.dev.bpe
     1000 fy.test.bpe
   221932 fy.train.bpe
   763287 fy.trainwithmono.bpe
   541355 mono-fy.fy.bpe
   541355 mono-fy.nl.bpe
      500 nl.dev.bpe
    32001 nl-fy.bpe
     1000 nl.test.bpe
   221932 nl.train.bpe
   763287 nl.trainwithmono.bpe
```
## Experiment 3: more data

In this experiment we run the pipeline after more trainings data has been collected until May 2021. We went from 221k (aligned) sentences to 1.1M.

* **BLEU nl->fy:** 51.5 (-0.9 cf. exp1, +2.5 above SMT baseline)
* **BLEU fy->nl:** 56.2 (+1.5 cf. exp1)

Sets (md5):

```
9326a232d1933ab812831b63ea0ae62e  fy.dev.txt
9570207cf8773c88153c7d4a902dd7c4  fy.train.txt
e02a0bc78419e23919cd8db75af66bf1  fy.test.txt
1e6e51ec3f257db519d0f162f3ca61c9  nl.dev.txt
807b84c19aa8c32edc39ca71c39f6e6e  nl.train.txt
75702fc8479291f7f53f862c885558fb  nl.test.txt
```

## Experiment 4: more data + filtering

There seemed to be a fair amount of contamination in the training data, mainly of dutch citations in Frisian text. We
performed a filtering step to filter this out (this removed around 78k sentences).

* **BLEU nl->fy:** 52.4 (+0.9 cf. exp3, +0.0 cf. exp1)
* **BLEU fy->nl:** 55.4 (-0.8 cf. exp3, +0.4 cf. exp1)

Sets (md5):

```
a50ae51d52b5297da29fac105f428890  fy.dev.txt
e02a0bc78419e23919cd8db75af66bf1  fy.test.txt
19a4d200128f9f3b3e5bc5dbcf4bdb81  fy.train.txt
d4f2656fa32e8c60dd931ca0dcf64871  nl.dev.txt
75702fc8479291f7f53f862c885558fb  nl.test.txt
73fa9d924e4fc8921111430932f4cfd5  nl.train.txt
```

## Experiment 5: larger dev and test set

Also applies the same filtering technique as appleid to exp 4

* Old test set (comparable with exp1-4)
    * **BLEU nl->fy:** 53.2 (+0.8 cf. exp4, +1.7 cf. exp3, +0.8 cf. exp1)
    * **BLEU fy->nl**: 56.0 (-0.2 cf exp3, +0.6 cf. exp4, +1.0 cf. exp1)
* New test set (5000 instances, not directly comparable with previous test sets):
    * **BLEU nl->fy:** 69.0
    * **BLEU fy->nl:** 72.5
* SMT experiment on the same filtered data (smt-exp5):
    * **BLEU nl->fy:** 61.7
    * **BLEU fy->nl:** 67.3

Sets (md5)

```
de4ba719ec7ebb3ebf93fb46ba6cda24  fy.dev.txt
724dc5789be18629610645ce33c46f8e  fy.test.txt
31a109108874ae3feb60cd9acf670366  fy.train.txt
faa8e1b0edc13f587e5afbe729705bad  nl.dev.txt
a5acc92f75a46915b80b74e54529f923  nl.test.txt
b8473c7eb0be7e219fbabe902bfbe608  nl.train.txt
```

## Experiment 6: Guided alignment

* New test set (5000 instances, only directly comparable with exp5):
    * **BLEU nl->fy:** 69.1
    * **BLEU fy->nl:** 72.5

Sets (md5)

```
de4ba719ec7ebb3ebf93fb46ba6cda24  fy.dev.txt
724dc5789be18629610645ce33c46f8e  fy.test.txt
31a109108874ae3feb60cd9acf670366  fy.train.txt
faa8e1b0edc13f587e5afbe729705bad  nl.dev.txt
a5acc92f75a46915b80b74e54529f923  nl.test.txt
b8473c7eb0be7e219fbabe902bfbe608  nl.train.txt
```
