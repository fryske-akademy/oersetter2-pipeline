#!/bin/bash

BASE=$(git rev-parse --show-toplevel)
if [ -z "$BASE" ] || [ ! -d "$BASE/data" ]; then
    echo "script must be run from the git repo" >&2
    exit 2
else
    BASE=$(realpath $BASE)
fi

echo "Switching to base directory $BASE">&2
cd $BASE/data

wget -c https://download.anaproy.nl/oersetter2-models.tar.gz || exit 1
tar --overwrite -xvzf oersetter2-models.tar.gz || exit 1
rm oersetter2-models.tar.gz

cd $BASE

