#!/bin/bash
if [ "$(hostname)" = "n-10-27-1-230" ]; then
    docker run --runtime=nvidia --rm -it -v /scratch/maartenvg/oersetter2-pipeline:/work registry.diginfra.net/mvg/oersetter2 bash -l
else
    echo "Not configured for this host">&2
    exit 2
fi
