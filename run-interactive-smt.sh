#!/bin/bash
if [ "$(hostname)" = "n-10-27-1-228" ] || [ "$(hostname)" = "n-10-27-1-229" ] || [ "$(hostname)" = "n-10-27-1-230" ]; then
    docker run --rm -it -v /scratch/maartenvg/oersetter2-pipeline:/work registry.diginfra.net/mvg/oersetter2-smt bash -l
else
    echo "Not configured for this host">&2
    exit 2
fi
