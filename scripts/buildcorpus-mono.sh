# Builds a monolingual corpus from the source documents

die() {
    echo "ERROR: $1" >&2
    exit 2
}

BASE=$(git rev-parse --show-toplevel)
if [ -z "$BASE" ]; then
    die "script must be run from the git repo"
else
    BASE=$(realpath $BASE)
fi

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8


export TGT=$1
[ -z "$TGT" ] && die "You must specify a target language"

echo "Checking dependencies" >&2
command -v ucto || die "ucto not found"

if [ -e "$BASE/data/source/mono/$TGT.skip" ]; then
    echo "WARNING: Skipping monolingual corpus generation, tokenised files must be manually provided!" >&2
else

    if [ ! -d "$BASE/data/source/mono/$TGT" ]; then
        die "Monolingual source data for $TGT is not found, did you clone all submodules and have the proper credentials to access the restricted data?"
    fi

    if [ "$TGT" = "nl" ]; then
        langparam="nld"
    elif [ "$TGT" = "fy" ]; then
        langparam="fry"
    else
        die "Invalid language in monolingual processing: $TGT"
    fi

    mkdir -p "$BASE/data/cache/mono/$TGT"

    echo "Building monolingual corpus for $TGT" >&2
    for f in "$BASE"/data/source/mono/$TGT/*.txt; do
        filename="$(basename -s .txt $f)"
        target="$BASE/data/cache/mono/$TGT/$filename.tok"
        if [ -f "$target" ]; then
            echo "$filename was already tokenised">&2
        else
            echo "Tokenising $filename">&2
            ucto -n -L "$langparam" "$f" "$target" || die "tokenisation failed"
        fi
    done

fi

cat $BASE/data/cache/mono/$TGT/*.tok | sed '/^$/d' > $BASE/data/cache/mono.$TGT.txt || die "unable to build mono corpus"
cat $BASE/data/$TGT.train.txt $BASE/data/cache/mono.$TGT.txt | sed '/^$/d' > $BASE/data/cache/trainwithmono.$TGT.txt || die "unable to build mono corpus with training part"
