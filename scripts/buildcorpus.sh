#!/bin/bash

# Builds the parallel corpus from the source documents

die() {
    echo "ERROR: $1" >&2
    exit 2
}

BASE=$(git rev-parse --show-toplevel)
if [ -z "$BASE" ]; then
    die "script must be run from the git repo"
else
    BASE=$(realpath $BASE)
fi

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

echo "Checking dependencies" >&2
command -v ucto || die "ucto not found"
command -v pandoc || die "pandoc not found"
command -v catdoc || die "catdoc not found"
command -v hunalign || die "hunalign not found"

if [ ! -d "$BASE/data/source/corpus" ]; then
    die "Corpus source data is not found, did you clone all submodules and have the proper credentials to access the restricted data?"
fi

mkdir -p "$BASE/data/cache/corpus" "$BASE/data/cache/corpus/prealigned"

docx2txt() {
    for f in "$1"/*.docx; do
        filename="$(basename -s .docx "$f")"
        target="$BASE/data/cache/corpus/$filename.txt"
        if [ -f "$target" ]; then
            echo "$filename was already converted">&2
        else
            echo "Converting $filename">&2
            pandoc -f docx -t plain -o "$target" "$f" || die "conversion failed"
        fi
    done
}

doc2txt() {
    for f in "$1"/*.doc; do
        filename="$(basename -s .doc "$f")"
        target="$BASE/data/cache/corpus/$filename.txt"
        if [ -f "$target" ]; then
            echo "$filename was already converted">&2
        else
            echo "Converting $filename">&2
            catdoc "$f" > "$target" || die "conversion failed"
        fi
    done
}


docx2txt "$BASE/data/source/corpus/"
docx2txt "$BASE/data/source/corpus/2021-08-12"
docx2txt "$BASE/data/source/corpus/2021-08-12/FNP"

doc2txt "$BASE/data/source/corpus/2021-08-12"
doc2txt "$BASE/data/source/corpus/materiaal_najaar2021"

tokenize() {
    for f in "$1"/*.txt; do
        filename="$(basename -s .txt "$f")"
        ndot="$(echo "$filename" | tr -dc '.' | awk '{ print length; }')"
        langfield=$((ndot + 1))
        lang="$(echo "$filename" | cut -d "." -f "$langfield")"
        if [ "$lang" = "nl" ]; then
            lang="nld"
        elif [ "$lang" = "fy" ]; then
            lang="fry"
        else
            die "Invalid language from $filename in tokenize(): $lang"
        fi
        target="$BASE/data/cache/corpus/$filename.tok"
        if [ -f "$target" ]; then
            echo "$filename was already tokenised">&2
        else
            echo "Tokenising $filename">&2
            ucto -n -L "$lang" "$f" "$target" || die "tokenisation failed"
        fi
    done
}

if [ "$SKIPCHECKS" != "1" ]; then
    tokenize "$BASE/data/cache/corpus"
    tokenize "$BASE/data/source/corpus/nieuwmateriaal"
    tokenize "$BASE/data/source/corpus/nieuwmateriaal/OMROP"
    tokenize "$BASE/data/source/corpus/2021-08-12"
    tokenize "$BASE/data/source/corpus/2021-08-12/omrop"
    tokenize "$BASE/data/source/corpus/2021-08-12/FNP"
    tokenize "$BASE/data/source/corpus/materiaal_najaar2021"
fi

count=$(ls "$BASE"/data/source/corpus/prealigned/*.txt | wc -l)
if [ $count -gt 0 ]; then
    for f in "$BASE"/data/source/corpus/prealigned/*.txt; do
        filename="$(basename -s .txt "$f")"
        ndot="$(echo "$filename" | tr -dc '.' | awk '{ print length; }')"
        langfield=$((ndot+1))
        lang="$(echo "$filename" | cut -d "." -f "$langfield")"
        if [ "$lang" = "nl" ]; then
            lang="nld"
        elif [ "$lang" = "fy" ]; then
            lang="fry"
        else
            die "Invalid language from $filename: $lang"
        fi
        target="$BASE/data/cache/corpus/prealigned/$filename.tok"
        if [ -f "$target" ]; then
            echo "$filename was already tokenised">&2
        else
            echo "Tokenising $filename">&2
            ucto -m -n -L "$lang" "$f" "$target" || die "tokenisation failed"
        fi
    done
fi

if [ "$SKIPCHECKS" != "1" ]; then
    batchfile="$BASE/data/cache/batch"
    rm "$batchfile" 2>/dev/null
    touch "$batchfile"
    touch "$BASE/data/cache/null.dic"

    echo "Creating batch job for sentence aligner">&2
    for f in "$BASE"/data/cache/corpus/*.nl.tok; do
        docname="$(basename -s .nl.tok "$f")"
        #hunalign -text "$BASE/data/cache/null.dic" "$BASE/data/cache/corpus/$docname.nl.tok" "$BASE/data/cache/corpus/$docname.fy.tok"
        echo -e "$BASE/data/cache/corpus/$docname.nl.tok\t$BASE/data/cache/corpus/$docname.fy.tok\t$BASE/data/cache/corpus/$docname.alignment" >> "$batchfile"
    done

    echo "Running sentence aligner">&2
    hunalign -text -batch "$BASE/data/cache/null.dic" "$batchfile"
fi


echo "Output corpus">&2
for f in "$BASE"/data/cache/corpus/*.alignment; do
    cat "$f" | awk -F '\t' '{ if ($1 != "" && $2 != "") { print $1; } }' >> "$BASE"/data/nl.txt
    cat "$f" | awk -F '\t' '{ if ($1 != "" && $2 != "") { print $2; } }' >> "$BASE"/data/fy.txt
done
for f in "$BASE"/data/cache/corpus/prealigned/*.nl.tok; do
    cat "$f" >> "$BASE"/data/nl.txt
done
for f in "$BASE"/data/cache/corpus/prealigned/*.fy.tok; do
    cat "$f" >> "$BASE"/data/fy.txt
done
