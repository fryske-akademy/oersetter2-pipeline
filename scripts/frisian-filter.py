#!/usr/bin/env python3

"""Ensures the parallel input on the Frisian side is not contaminated with Dutch"""

import argparse
import sys

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--nl','-n',type=str, help="Dutch part", action='store', required=True)
    parser.add_argument('--fy','-f',type=str, help="Frisian part", action='store', required=True)
    parser.add_argument('--omitlist',type=str, help="word occcuring in this list are omitted", action='store', required=True)
    parser.add_argument('--dry','-x',help="Dry run, don't write files", action='store_true')
    args = parser.parse_args()


    omitlist = set()
    with open(args.omitlist, 'r', encoding='utf-8') as f_in:
        for line in f_in:
            word = line.split("\t")[0]
            omitlist.add(word.lower())

    f_nl = open(args.nl,'r',encoding='utf-8')
    f_fy = open(args.fy,'r',encoding='utf-8')
    print("Writing to " + args.nl  + ".filtered and " + args.fy + ".filtered",file=sys.stderr)
    if not args.dry:
        f_nl_out = open(args.nl + ".filtered",'w',encoding='utf-8')
        f_fy_out = open(args.fy + ".filtered",'w',encoding='utf-8')
    for line_nl, line_fy in zip(f_nl, f_fy):
        omit = False
        if line_nl.strip() == line_fy.strip():
            omit = True
            print("Sentences are equal, omitting: ", line_nl.strip(), " -- " , line_fy.strip(),file=sys.stderr)
        elif line_fy.find("\"") != -1:
            parts_fy = line_fy.split("\"")
            parts_nl = line_nl.split("\"")
            if len(parts_fy) == len(parts_nl):
                for part_fy, part_nl in zip(parts_fy, parts_nl):
                    part_fy = part_fy.strip()
                    part_nl = part_nl.strip()
                    if len(part_fy) > 15 and len(part_nl) > 15 and ' ' in part_fy:
                        if part_fy == part_nl:
                            omit = True
                            print("Sentences contain quotes and a large equal subpart, omitting: ", line_nl.strip(), " -- " , line_fy.strip(),file=sys.stderr)
                            break
        if not omit:
            nonfrisian = []
            for word in line_fy.split(" "):
                word = word.lower()
                if word in omitlist:
                    nonfrisian.append(word)

            nonfrisianratio = len(nonfrisian) / len(line_fy)
            if nonfrisianratio >= 0.1:
                print(f"Sentence contains " + str(round(nonfrisianratio*100)) + "% non-frisian words ("  + repr(nonfrisian) + "), omitting: ", line_nl.strip(), " -- " , line_fy.strip(),file=sys.stderr)
                omit = True

        if not omit and not args.dry:
            f_nl_out.write(line_nl)
            f_fy_out.write(line_fy)

f_nl_out.close()
f_fy_out.close()
