#!/usr/bin/env python3

import argparse
import sys


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--nl','-n',type=str, help="Dutch frequency lexicon (tsv)", action='store', required=True)
    parser.add_argument('--fy','-f',type=str, help="Frisian wordlist", action='store', required=True)
    parser.add_argument('--threshold','-t',type=int, help="Absolute frequency threshold", action='store', default=20,required=True)
    args = parser.parse_args()

    fy = set()
    with open(args.fy,'r',encoding='utf-8') as f_fy:
        for line in f_fy:
            fy.add(line.strip().lower())

    with open(args.nl,'r',encoding='utf-8') as f_nl:
        for line in f_nl:
            fields = line.strip().split("\t")
            if len(fields) == 2:
                word = fields[0].lower()
                if word.isalpha() and int(fields[1]) >= args.threshold:
                    if word not in fy:
                        print(word + "\t" + fields[1])
                    else:
                        print(word + " can also be frisian",file=sys.stderr)


