#!/bin/bash
#this script must be sourced

command -v ucto || die "ucto not found"

echo "Preprocessing monolingual corpus $TGT" >&2
if [ ! -f "data/models/mono-$TGT.$TGT.bpe" ]; then
    subword-nmt apply-bpe -c data/models/$SRC-$TGT.bpe < "data/cache/mono.$TGT.txt" > "data/models/mono-$TGT.$TGT.bpe" || die "apply bpe failed"
fi
