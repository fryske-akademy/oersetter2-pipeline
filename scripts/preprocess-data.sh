#!/bin/bash

die() {
    echo "ERROR: $1" >&2
    exit 2
}

BASE=$(git rev-parse --show-toplevel)
if [ -z "$BASE" ]; then
    die "script must be run from the git repo"
else
    BASE=$(realpath $BASE)
fi

command -v subword-nmt || die "subword-nmt not found"
command -v ssam || die "ssam not found"

# number of merge operations
bpe_operations=32000

mkdir -p "$BASE/data/log"

cd "$BASE"/data
if [ -e nl.test.txt ] && [ -e fy.test.txt ]  && [ -e nl.dev.txt ] && [ -e fy.dev.txt ]; then
    echo "Test and dev sets already exist, retaining them as-is and expanding only trainingsdata" >&2
    if [ -e nl.prefilter.dev.txt ] && [ -e nl.prefilter.test.txt ]; then
        echo "(using prefiltered data)" >&2
        cat nl.prefilter.test.txt nl.prefilter.dev.txt > nl.testdev.txt
        cat fy.prefilter.test.txt fy.prefilter.dev.txt > fy.testdev.txt || die "prefiltered files gone missing?"
        rm nl.prefilter.train.txt nl.prefilter.dev.txt
        rm fy.prefilter.train.txt fy.prefilter.dev.txt
    elif [ -e nl.prefilter.dev.txt ] && [ -e fy.prefilter.dev.txt ]; then
        echo "(using prefiltered data)" >&2
        cat nl.test.txt nl.prefilter.dev.txt > nl.testdev.txt
        cat fy.test.txt fy.prefilter.dev.txt > fy.testdev.txt || die "prefiltered files gone missing?"
        rm nl.prefilter.train.txt nl.prefilter.dev.txt
        rm fy.prefilter.train.txt fy.prefilter.dev.txt
    else
        echo "(no prefiltered data available)" >&2
        cat nl.test.txt nl.dev.txt > nl.testdev.txt
        cat fy.test.txt fy.dev.txt > fy.testdev.txt
    fi
    [ -f source/sets/fy.test202101.txt ] && cat source/sets/fy.test202101.txt >> fy.testdev.txt
    [ -f source/sets/nl.test202101.txt ] && cat source/sets/nl.test202101.txt >> nl.testdev.txt
    ssam --shuffle --sizes "*" --seed 12345 --exclude fy.testdev.txt,nl.testdev.txt --names "train" fy.txt nl.txt || die "failed to split sets"
else
    echo "Split the data into sets" >&2

    if [ "$OLDTESTSET" = 1 ]; then
        ssam --shuffle --sizes "1000,1000,*" --seed 12345 --names "test,dev,train" fy.txt nl.txt || die "failed to split sets"
    else
        # exclude the test test from january and make a bigger new one
        ssam --shuffle --sizes "5000,5000,*" --seed 12345 --names "test,dev,train" --exclude source/sets/fy.test202101.txt,source/sets/nl.test202101.txt fy.txt nl.txt || die "failed to split sets"
    fi

    if [ ! -e nl.prefilter.test.txt ] || [ ! -e fy.prefilter.test.txt ]; then
        echo "Applying filter to test sets" >&2
        $BASE/scripts/frisian-filter.py --nl nl.test.txt --fy fy.test.txt --omitlist "$BASE/data/source/lexicons/nonfrisianwords.nl.tsv" 2> "$BASE/data/log/filter.test.log" || die "filter on test set failed"
        mv nl.test.txt nl.prefilter.test.txt
        mv fy.test.txt fy.prefilter.test.txt
        mv nl.test.txt.filtered nl.test.txt || die "filtered output not found for nl.test"
        mv fy.test.txt.filtered fy.test.txt || die "filtered output not found for fy.test"
    fi
fi

if [ ! -e nl.prefilter.train.txt ] || [ ! -e fy.prefilter.train.txt ]; then
    echo "Applying filter to train set" >&2
    $BASE/scripts/frisian-filter.py --nl nl.train.txt --fy fy.train.txt --omitlist "$BASE/data/source/lexicons/nonfrisianwords.nl.tsv" 2> "$BASE/data/log/filter.train.log" || die "filter on training set failed"
    mv nl.train.txt nl.prefilter.train.txt
    mv fy.train.txt fy.prefilter.train.txt
    mv nl.train.txt.filtered nl.train.txt || die "filtered output not found for nl.train"
    mv fy.train.txt.filtered fy.train.txt || die "filtered output not found for fy.train"
fi


if [ ! -e nl.prefilter.dev.txt ] || [ ! -e fy.prefilter.dev.txt ]; then
    echo "Applying filter to dev set" >&2
    $BASE/scripts/frisian-filter.py --nl nl.dev.txt --fy fy.dev.txt --omitlist "$BASE/data/source/lexicons/nonfrisianwords.nl.tsv" 2> "$BASE/data/log/filter.dev.log" || die "filter on dev set failed"
    mv nl.dev.txt nl.prefilter.dev.txt
    mv fy.dev.txt fy.prefilter.dev.txt
    mv nl.dev.txt.filtered nl.dev.txt || die "filtered output not found for nl.dev"
    mv fy.dev.txt.filtered fy.dev.txt || die "filtered output not found for fy.dev"
fi

mkdir -p source/sets
cp *.train.txt *.test.txt *.dev.txt source/sets/

mkdir -p models

echo "Learn Byte-Pair Encoding on training data" >&2
cat nl.train.txt fy.train.txt | subword-nmt learn-bpe -s $bpe_operations > models/nl-fy.bpe || die "learning bpe failed"

echo "Apply Byte-Pair Encoding on data" >&2
#apply BPE
for settype in train dev test; do
    subword-nmt apply-bpe -c models/nl-fy.bpe < "nl.$settype.txt" > "models/nl.$settype.bpe" || die "apply bpe failed"
    subword-nmt apply-bpe -c models/nl-fy.bpe < "fy.$settype.txt" > "models/fy.$settype.bpe" || die "apply bpe failed"
done


