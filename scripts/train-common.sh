# do not call this script directly, it should be sourced

# shellcheck disable=SC2086   #don't force quoting
# shellcheck disable=SC2002   #useless cat

die() {
    echo "ERROR: $1" >&2
    exit 2
}

usage() {
    echo "Usage: train-(nmt|smt).sh [options] -s [sourcelang] -t [targetlang] -n [experiment name]">&2
    echo "       sourcelang/targetlang are nl or fy">&2
    echo "Options:">&2
    echo "       -T   Rebuild and expand trainings data with new possible new material, but retain test and dev set as-is">&2
    echo "       -o   Use older testset from 2021-01">&2
    echo "       -b   Enable backwards enrichment of training data using monolingual data (NMT only)">&2
    echo "       -m   Model type (transformer, s2s) (NMT only)">&2
    echo "       -C   Skip corpus build checks (tokenisation/alignment) (speeds things up a bit if you're sure it's done already)">&2
    exit 0
}

evaluate() {
    cat data/cache/$NAME.$TGT.test.rawoutput.txt | sed 's/\@\@ //g' \
        | $MOSES_SCRIPT_DIR/recaser/detruecase.perl \
        | $MOSES_SCRIPT_DIR/tokenizer/detokenizer.perl -l nl > data/output/$NAME.$TGT.test.output.txt || die "Postprocessing of output failed" #we use the same language for dutch and frisian, the detokeniser is not aware of either anyway

    if [ ! -s "data/output/$NAME.$TGT.test.output.txt" ]; then
        die "Output file is empty after processing"
    fi

    mkdir -p data/evaluation
    echo "Evaluate test set">&2
    LC_ALL=C.UTF-8 sacrebleu -l $SRC-$TGT $BASE/data/$TGT.test.txt < $BASE/data/output/$NAME.$TGT.test.output.txt | tee "$BASE/data/evaluation/$NAME.$SRC-$TGT.test.eval.txt"
}

SRC="nl"
TGT="fy"
NAME="exp"
export MODELTYPE="transformer"
export OLDTESTSET=0
BACK=0
export SKIPCHECKS=0
NEWMATERIAL=0

while getopts "h:s:t:n:m:TbCo" options; do
    case "${options}" in
        h)
            usage
            ;;
        s)
            SRC=${OPTARG}
            ;;
        t)
            TGT=${OPTARG}
            ;;
        n)
            NAME=${OPTARG}
            ;;
        m)
            export MODELTYPE=${OPTARG}
            ;;
        T)
            NEWMATERIAL=1
            ;;
        b)
            BACK=1
            ;;
        o)
            export OLDTESTSET=1
            ;;
        C)
            export SKIPCHECKS=1
            ;;
        :)
            echo "Error -${OPTARG} requires an argument"
            exit 1
            ;;
        *)
            exit 1
            ;;
    esac
done



BASE=$(git rev-parse --show-toplevel)
if [ -z "$BASE" ] || [ ! -d "$BASE/data" ]; then
    die "script must be run from the git repo"
else
    BASE=$(realpath $BASE)
fi

echo "Switching to base directory $BASE">&2
cd $BASE

if [ ! -e "$BASE/data/models" ]; then
    die "models have not been downloaded yet, doing so now..."
    $BASE/download-models.sh || die "failed to download models"
fi

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8


if [ "$MONO" != "1" ]; then
    MONO=$BACK
fi

if [ -n "$NEWMATERIAL" ] && [ $NEWMATERIAL -eq 1 ]; then
    echo "Preparing adding new material to training data whilst leaving other sets intact...">&2
    rm "$BASE"/data/$SRC.txt
    rm "$BASE"/data/$TGT.txt
    rm "$BASE"/data/models/*.bpe
    rm "$BASE"/data/cache/train-for-align.txt
fi

if [ ! -e "$BASE"/data/nl.txt ] || [ ! -e "$BASE"/data/fy.txt ]; then
    ./scripts/buildcorpus.sh  || die "building corpus failed"
else
    echo "(corpus already built)">&2
fi

if [ ! -e "$BASE"/data/models/nl.train.bpe ]; then
    ./scripts/preprocess-data.sh  || die "preprocessing failed"
else
    echo "(preprocessing already done)">&2
fi

if [ $MONO -eq 1 ] && [ ! -e "$BASE"/data/cache/mono.$TGT.txt ]; then
    ./scripts/buildcorpus-mono.sh $TGT || die "building mono corpus $TGT failed"
elif [ $MONO -eq 1 ]; then
    echo "(monolingual corpus already built)">&2
else
    echo "(no monolingual corpus needed)">&2
fi

if [ $MONO -eq 1 ] && [ ! -e "$BASE"/data/models/mono-$TGT.$TGT.bpe ]; then
    # shellcheck disable=SC1091
    source ./scripts/preprocess-data-mono.sh  || die "preprocessing failed"
elif [ $MONO -eq 1 ]; then
    echo "(monolingual preprocessing already done)">&2
else
    echo "(no monolingual preprocessing needed)">&2
fi

MOSES_SCRIPT_DIR=/usr/src/mosesdecoder/scripts/
MOSES_DECODER_BIN=$(realpath "$MOSES_SCRIPT_DIR/../bin/moses") #verify, only for SMT

if [ ! -e "$MOSES_SCRIPT_DIR" ]; then
    die "unable to find moses scripts"
fi
