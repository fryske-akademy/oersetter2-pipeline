#!/bin/bash

#(partially derived from https://raw.githubusercontent.com/marian-nmt/marian-examples/master/transformer/run-me.sh)

# shellcheck disable=SC2086   #don't force quoting
# shellcheck disable=SC2002   #useless cat

# shellcheck disable=SC1091
if ! source scripts/train-common.sh; then
    echo "Unable to find scripts/train-common.sh , make sure your current working directory is the repository root directory">&2
    exit 1
fi

WORKSPACE=10000
MAXEPOCHS=60
BEAMSIZE=12
SEED=1111
GPUS="0"
ENCODER_LAYERS=6
DECODER_LAYERS=6
TRANSFORMER_HEADS=8

command -v marian || die "marian not found"
command -v marian-decoder || die "marian-decoder not found"
command -v marian-vocab || die "marian-vocab not found"
command -v marian-scorer || die "marian-scorer not found"

mkdir -p data/output
mkdir -p data/log

echo "Create common vocabulary">&2
if [ ! -e "data/models/vocab.nl-fy.yml" ] || [ $NEWMATERIAL -eq 1 ]; then
    #does not depend on translation direction
    cat data/models/nl.train.bpe data/models/fy.train.bpe | marian-vocab --max-size 36000 > data/models/vocab.nl-fy.yml
else
    echo "(vocabulary already created)">&2
fi

#Train word alignment/phrase alignment for guided alignment
if [ ! -f "data/cache/train-for-align.txt" ]; then
    echo "Gathering training data for guided alignment...">&2
    paste data/$SRC.train.txt data/$TGT.train.txt | sed 's/\t/ ||| /g' > data/cache/train-for-align.txt || die "unable to prepare data for alignment"
    #force recomputation of next step
    rm "data/cache/$SRC-$TGT.align" 2> /dev/null
    rm "data/cache/$TGT-$SRC.align" 2> /dev/null
fi

if [ ! -f "data/cache/$SRC-$TGT.align" ] || [ ! -f "data/cache/$TGT-$SRC.align" ]; then
    echo "Computing forward alignment...">&2
    fast_align -vdo -i data/cache/train-for-align.txt > data/cache/$SRC-$TGT.align || die "forward alignment using fast_align failed"
    echo "Computing reverse alignment...">&2
    fast_align -vdor -i data/cache/train-for-align.txt > data/cache/$TGT-$SRC.align || die "reverse alignment using fast_align failed"
    #force recomputation of next step
    rm "data/cache/$SRC-$TGT.grow-diag-final.align" 2> /dev/null
fi

if [ ! -f "data/cache/$SRC-$TGT.grow-diag-final.align" ]; then
    echo "Computing grow-diag-final alignment...">&2
    atools -c grow-diag-final -i data/cache/$SRC-$TGT.align -j data/cache/$TGT-$SRC.align > data/cache/$SRC-$TGT.grow-diag-final.align || die "grow-diag-final alignment failed"
fi

if [ $BACK -eq 1 ]; then
    mkdir -p "data/models/backwards-$TGT"
    #train backwards model: translates target to source on a monolingual data collection
    #to synthetically enrich the source corpus
    echo "Train backwards model to enrich training input">&2
    if [ ! -e "data/models/backwards-$TGT/model.npz.best-translation.npz" ]
    then
        rm data/log/$SRC-$TGT.back.train.log 2>/dev/null
        rm data/log/$SRC-$TGT.back.valid.log 2>/dev/null
        marian \
            --model data/models/backwards-$TGT/model.npz --type s2s \
            --train-sets data/models/$TGT.train.bpe data/models/$SRC.train.bpe \
            --max-length 100 \
            --vocabs data/models/vocab.nl-fy.yml data/models/vocab.nl-fy.yml \
            --mini-batch-fit -w 3500 --maxi-batch 1000 \
            --valid-freq 10000 --save-freq 10000 --disp-freq 1000 \
            --valid-metrics ce-mean-words perplexity translation \
            --valid-sets data/models/$TGT.dev.bpe data/models/$SRC.dev.bpe \
            --valid-script-path "bash scripts/validate.$SRC.sh" \
            --valid-translation-output data/output/$SRC.dev.out.bpe --quiet-translation \
            --valid-mini-batch 64 --beam-size 12 --normalize=1 \
            --overwrite --keep-best \
            --early-stopping 5 --after-epochs 15 --cost-type=ce-mean-words \
            --log data/log/$SRC-$TGT.back.train.log --valid-log data/log/$SRC-$TGT.back.valid.log \
            --tied-embeddings-all --layer-normalization \
            --devices $GPUS --seed 1$SEED \
            --exponential-smoothing
    else
        echo "(already trained)">&2
    fi

    echo "Apply backwards model to enrich training input">&2
    if [ ! -e "data/models/mono-$TGT.$SRC.bpe" ]
    then
        marian-decoder \
          -c data/models/backwards-$TGT/model.npz.best-translation.npz.decoder.yml \
          -i data/models/mono-$TGT.$TGT.bpe \
          -b 6 --normalize=1 -w 2500 -d $GPUS \
          --mini-batch 64 --maxi-batch 100 --maxi-batch-sort src \
          --max-length 200 --max-length-crop \
          > data/models/mono-$TGT.$SRC.bpe
    else
        echo "(already applied)">&2
    fi

    if [ ! -e "data/models/$SRC.trainwithmono.bpe" ]; then
        cat data/models/$SRC.train.bpe data/models/mono-$TGT.$SRC.bpe > data/models/$SRC.trainwithmono.bpe
        cat data/models/$TGT.train.bpe data/models/mono-$TGT.$TGT.bpe > data/models/$TGT.trainwithmono.bpe
    fi

    TRAINSRC="data/models/$SRC.trainwithmono.bpe"
    TRAINTGT="data/models/$TGT.trainwithmono.bpe"
else
    TRAINSRC="data/models/$SRC.train.bpe"
    TRAINTGT="data/models/$TGT.train.bpe"
fi

# train model (transformer): source->target
echo "Train model">&2
mkdir -p data/models/$NAME
if [ ! -e "data/models/$NAME/$SRC-$TGT.model.npz" ]; then
    rm data/log/$NAME.$SRC-$TGT.train.log 2>/dev/null
    rm data/log/$NAME.$SRC-$TGT.valid.log 2>/dev/null
    if [ "$MODELTYPE" = "transformer" ]; then
        marian \
            --model data/models/$NAME/$SRC-$TGT.model.npz --type transformer \
            --train-sets $TRAINSRC $TRAINTGT \
            --max-length 100 \
            --vocabs data/models/vocab.nl-fy.yml data/models/vocab.nl-fy.yml \
            --mini-batch-fit -w $WORKSPACE --maxi-batch 1000 \
            --early-stopping 10 --after-epochs $MAXEPOCHS --cost-type=ce-mean-words \
            --valid-freq 5000 --save-freq 5000 --disp-freq 500 \
            --valid-metrics ce-mean-words perplexity translation \
            --valid-sets data/models/$SRC.dev.bpe data/models/$TGT.dev.bpe \
            --valid-script-path "bash scripts/validate.$TGT.sh" \
            --valid-translation-output data/output/$TGT.dev.out.bpe --quiet-translation \
            --valid-mini-batch 64 \
            --overwrite --keep-best \
            --beam-size $BEAMSIZE --normalize 0.6 \
            --log data/log/$NAME.$SRC-$TGT.train.log --valid-log data/log/$NAME.$SRC-$TGT.valid.log \
            --enc-depth $ENCODER_LAYERS --dec-depth $DECODER_LAYERS \
            --transformer-heads $TRANSFORMER_HEADS \
            --transformer-postprocess-emb d \
            --transformer-postprocess dan \
            --transformer-dropout 0.1 --label-smoothing 0.1 \
            --learn-rate 0.0003 --lr-warmup 16000 --lr-decay-inv-sqrt 16000 --lr-report \
            --optimizer-params 0.9 0.98 1e-09 --clip-norm 5 \
            --tied-embeddings-all \
            --devices $GPUS --sync-sgd --seed $SEED \
            --exponential-smoothing \
            --guided-alignment data/cache/$SRC-$TGT.grow-diag-final.align || die "training failed"
    elif [ "$MODELTYPE" = "s2s" ]; then
        marian \
            --model data/models/$NAME/$SRC-$TGT.model.npz --type s2s \
            --train-sets $TRAINSRC $TRAINTGT \
            --max-length 100 \
            --vocabs data/models/vocab.nl-fy.yml data/models/vocab.nl-fy.yml \
            --mini-batch-fit -w $WORKSPACE --maxi-batch 1000 \
            --early-stopping 10 --after-epochs $MAXEPOCHS --cost-type=ce-mean-words \
            --valid-freq 5000 --save-freq 5000 --disp-freq 500 \
            --valid-metrics ce-mean-words perplexity translation \
            --valid-sets data/models/$SRC.dev.bpe data/models/$TGT.dev.bpe \
            --valid-script-path "bash scripts/validate.$TGT.sh" \
            --valid-translation-output data/output/$TGT.dev.out.bpe --quiet-translation \
            --valid-mini-batch 64 \
            --overwrite --keep-best \
            --beam-size $BEAMSIZE --normalize 0.6 \
            --log data/log/$NAME.$SRC-$TGT.train.log --valid-log data/log/$NAME.$SRC-$TGT.valid.log \
            --enc-depth $ENCODER_LAYERS --dec-depth $DECODER_LAYERS \
            --learn-rate 0.0003 --lr-warmup 16000 --lr-decay-inv-sqrt 16000 --lr-report \
            --optimizer-params 0.9 0.98 1e-09 --clip-norm 5 \
            --tied-embeddings-all \
            --devices $GPUS --sync-sgd --seed $SEED \
            --exponential-smoothing \
            --guided-alignment data/cache/$SRC-$TGT.grow-diag-final.align || die "training failed"
    else
        die "Invalid modeltype: $MODELTYPE"
    fi
else
    echo "(model already trained)">&2
fi

#echo "Find best model on development set">&2
#ITER=$(cat "$BASE/data/log/$SRC-$TGT.valid.log" | grep translation | sort -rg -k12,12 -t' ' | cut -f8 -d' ' | head -n1)

echo "Translate test sets">&2
cat data/models/$SRC.test.bpe \
    | marian-decoder -c data/models/$NAME/$SRC-$TGT.model.npz.decoder.yml -m data/models/$NAME/$SRC-$TGT.model.npz.best-translation.npz -d $GPUS -b $BEAMSIZE -w 6000 --normalize 0.6 --mini-batch 64 --maxi-batch-sort src --maxi-batch 100 --allow-unk -o data/cache/$NAME.$TGT.test.rawoutput.txt || die "Translation of test set failed"

if [ ! -s "data/cache/$NAME.$TGT.test.rawoutput.txt" ]; then
    die "Output file is empty after decoding test set"
fi

evaluate
