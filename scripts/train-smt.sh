#!/bin/bash

export MONO=1 #enable monolingual processing (for language model)
if ! source scripts/train-common.sh; then
    echo "Unable to find scripts/train-common.sh , make sure your current working directory is the repository root directory">&2
    exit 1
fi

SRILM_BIN_DIR=/usr/src/srilm/bin/i686-m64/ #only used for SMT
MGIZA_BIN_DIR=/usr/src/mgiza/mgizapp/bin/ #only used for SMT


[ -x "$SRILM_BIN_DIR/ngram-count" ] || die "SRILM ngram-count not found"
[ -x "$MOSES_DECODER_BIN" ] || die "moses decoder not found"
[ -e "$MOSES_SCRIPT_DIR/training/train-model.perl" ] || die "moses training script not found"
[ -e "$MOSES_SCRIPT_DIR/training/mert-moses.pl" ] || die "moses tuning script not found"
[ -e "$MGIZA_BIN_DIR/mgiza" ] || die "mgiza not not found"
command -v python3 || die "python3 not found"
command -v python || ln -s /usr/bin/python3 /usr/bin/python || die "unable to link /usr/bin/python to /usr/bin/python3" #simply link python to python3

[ -e "$MGIZA_BIN_DIR/merge_alignment.py" ] || cp /usr/src/mgiza/mgizapp/scripts/merge_alignment.py "$MGIZA_BIN_DIR" || die "unable to find or install merge_alignment.py"


export PATH="$MGIZA_BIN_DIR:$SRILM_BIN_DIR:$PATH"

mkdir -p data/output
mkdir -p data/log

echo "Building language model">&2
if [ ! -e "$BASE"/data/cache/trainwithmono.$TGT.lm ]; then
    $SRILM_BIN_DIR/ngram-count -interpolate -kndiscount -unk -order 3 -text "$BASE"/data/cache/trainwithmono.$TGT.txt -lm "$BASE"/data/cache/trainwithmono.$TGT.lm || die "unable to build language model"
else
    echo "(language model already built, skipping)">&2
fi

echo "Training translation model">&2

mkdir -p data/cache/work-smt.$SRC-$TGT.$NAME
cd data/cache/work-smt.$SRC-$TGT.$NAME
rm -Rf corpus
mkdir corpus
ln -s "$BASE/data/$SRC.train.txt" "corpus/train.$SRC"  || die "unable to link to source data"
ln -s "$BASE/data/$TGT.train.txt" "corpus/train.$TGT"  || die "unable to link to target data"

if [ ! -e model/phrase-table.gz ]; then
    LOGFILE="$BASE/data/log/$SRC-$TGT.$NAME.train-smt.log"
    $MOSES_SCRIPT_DIR/training/train-model.perl  -external-bin-dir "$MGIZA_BIN_DIR" --mgiza --root-dir . --corpus corpus/train --f $SRC --e $TGT --lm "0:3:$BASE/data/cache/trainwithmono.$TGT.lm" --parallel 2> "$LOGFILE" >&2 || (
        tail $LOGFILE;
        die "training failed, inspect $LOGFILE for further details"
        false
    ) || exit 3
else
    echo "(already trained, skipping)">&2
fi

echo "Tuning translation model (MERT)">&2

if [ ! -d "mert-work" ]; then
    threads=$(($(nproc) - 4))
    MOSES_MERT_DIR=$(realpath "$MOSES_SCRIPT_DIR/../mert")
    LOGFILE="$BASE/data/log/$SRC-$TGT.$NAME.mert.log"
    $MOSES_SCRIPT_DIR/training/mert-moses.pl  --mertdir=$MOSES_MERT_DIR --rootdir=$MOSES_SCRIPT_DIR --predictable-seeds --decoder-flags "-threads $threads -v 0" "$BASE"/data/$SRC.dev.txt "$BASE"/data/$TGT.dev.txt "$MOSES_DECODER_BIN" model/moses.ini 2> "$LOGFILE" >&2 || (
        tail $LOGFILE;
        die "tuning failed, inspect $LOGFILE for further details"
        false
    ) || exit 3
    cp mert-work/moses.ini moses.tuned.ini || die "can't copy tuned configuration"
else
    echo "(already tuned, skipping)">&2
fi


echo "Translate test sets">&2
$MOSES_DECODER_BIN -f moses.tuned.ini < "$BASE/data/$SRC.test.txt" > "$BASE/data/cache/$NAME.$TGT.test.rawoutput.txt" 2> "$BASE/data/log/$SRC-$TGT.$NAME.test.log" || die "test stage failed"

cd -

if [ ! -s "$BASE/data/cache/$NAME.$TGT.test.rawoutput.txt" ]; then
    die "Output file is empty after decoding test set"
fi

evaluate
