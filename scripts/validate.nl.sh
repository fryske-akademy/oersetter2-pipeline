#!/bin/bash

MOSES_SCRIPT_DIR=/usr/src/mosesdecoder/scripts/

if [ ! -e "$MOSES_SCRIPT_DIR" ]; then
    echo "unable to find moses scripts">&2
    exit 1
fi

cat $1 \
    | sed 's/\@\@ //g' \
    | $MOSES_SCRIPT_DIR/recaser/detruecase.perl 2>/dev/null \
    | $MOSES_SCRIPT_DIR/tokenizer/detokenizer.perl -l nl 2>/dev/null \
    | $MOSES_SCRIPT_DIR/generic/multi-bleu-detok.perl data/nl.dev.txt \
    | sed -r 's/BLEU = ([0-9.]+),.*/\1/'
