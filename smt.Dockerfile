FROM ubuntu:20.04
MAINTAINER Maarten van Gompel <proycon@anaproy.nl>
LABEL description="Oersetter SMT"
ENV TZ=Europe/Amsterdam
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update
RUN apt-get -y install git sudo ucto gcc g++ autoconf-archive make libtool build-essential libboost-all-dev libprotobuf17 protobuf-compiler libprotobuf-dev openssl libssl-dev libgoogle-perftools-dev gawk sed grep gnupg2 curl ca-certificates doxygen pandoc perl python3 python3-pip python3-dev cargo rustc libxmlrpc-c++8-dev libxmlrpc-c++8v5 libxmlrpc-core-c3-dev cmake wget bzip2 libbz2-dev zlib1g-dev zlibc libicu-dev liblzma-dev

RUN mkdir -p /usr/src/

#hunalign
RUN cd /usr/src && git clone https://github.com/danielvarga/hunalign.git
RUN cd /usr/src/hunalign/src/hunalign && make && cp hunalign /usr/bin

#mgiza
RUN cd /usr/src && git clone https://github.com/moses-smt/mgiza

RUN cd /usr/src/mgiza/mgizapp && cmake . && make && sudo make install

RUN mkdir -p /usr/src/srilm

#SRILM (do not use this copy for anything else, there is a license you need to accept for SRILM)
RUN cd /usr/src/srilm && wget https://download.anaproy.nl/srilm-1.7.3.tar.gz && tar -xzf srilm*gz

RUN cd /usr/src/srilm && make SRILM=/usr/src/srilm

#Moses SMT
RUN cd /usr/src && git clone https://github.com/moses-smt/mosesdecoder

RUN cd /usr/src/mosesdecoder && ./bjam -j6

#Subword Neural Machine Translation (preprocessing to segment text into subword units)
RUN sudo pip3 install subword-nmt

#Evaluation scripts
RUN sudo pip3 install sacrebleu

#Install ssam
RUN cargo install ssam && cp -f /root/.cargo/bin/ssam /usr/local/bin/ssam

#Set locales
RUN apt-get -y install language-pack-en language-pack-nl
ENV LC_ALL=en_US.UTF-8

#Output defaults to profile template in case other users are generated
RUN echo "export LANG=en_US.UTF-8" >> /etc/skel/.profile
RUN echo "export LC_ALL=en_US.UTF-8" >> /etc/skel/.profile
RUN echo "source ~/.bashrc" >> /etc/skel/.profile

CMD /bin/bash -l
