#!/bin/bash

echo "Note: this script requires specific credentials and is intended only for the oersetter maintainers">&2

BASE=$(git rev-parse --show-toplevel)
if [ -z "$BASE" ] || [ ! -d "$BASE/data" ]; then
    die "script must be run from the git repo"
else
    BASE=$(realpath $BASE)
fi

echo "Switching to base directory $BASE">&2
cd $BASE/data

rm oersetter2-models.tar.gz 2> /dev/null
tar -cvzf oersetter2-models.tar.gz models || exit 1
scp oersetter2-models.tar.gz proycon@anaproy.nl:/nettmp/ || exit 1

cd $BASE

